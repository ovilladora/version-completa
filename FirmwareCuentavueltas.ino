
#include <TimerOne.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>

const int rs = 11, en = 12, d4 = 13, d5 = 14, d6 = 15, d7 = 16;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////    VARIABLES Y DEFINICIONES    ///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool trabajando=0;
long int vel1=0;
unsigned int velocidad=0,cantVueltas=0,cantVueltasAnt=0;
unsigned long int tPuntoAnt=0,refresh1=0,refresh2=0;
signed int posPunto=0,cont_rotary=0,cont_ant,cont_act;
volatile int vueltas=0;

/*  Definicion de hardware    */
#define sensor        2               //IRQ 1  <--------------- NO SE PUEDE MODIFICAR EL NUMERO DE PIN
#define rotary_A      3               //IRQ 2  <--------------- NO SE PUEDE MODIFICAR EL NUMERO DE PIN
#define rotary_B      4               
#define dir1          5 
#define step1         6 
#define enable_pin    7 
#define enter         18
#define backlight     17              //pin 26 atmega   A3 en arduino
#define vcc_sensor    19              //pin28 atmega A5 en arduino 

/* fin definiciones de hardware */

/* constantes de configuracion */
#define AUTO_DISABLE          true             //si esta asignado 'true' se deshabilita el motor al terminar o estar en pausa,si esta en 'false' sigue energizado bloqueando al motor cuando no esta girando
#define DIR_MOTOR             HIGH              //configuracion del sentido de giro del motor, puede asignarse HIGH o LOW
#define LONGBUTTON            200               //tiempo de pulsacion para considerar una pulsacion larga en ms 
#define VUELTAS_MAX           1000              //cantidad de vueltas maximas que puede elegir el usuario
#define VUELTAS_DEFAULT       10                //cantidad de vueltas por defecto seteadas
#define VEL_MAX               10                //el rango es de 1 a 10
#define VEL_DEFAULT           1                 //valor por defecto de la velocidad [1-10]
#define VEL_MAX_CUENTAS       400//100               //velocidad maxima del motor en us
#define VEL_MIN_CUENTAS       2400//600              //velocidad minima del motor en us
#define VEL_DEFAULT_CUENTAS   2200              //valor por defecto de la velocidad en us del timer
#define T_PUNTO 500                             //velocidad a la que se desplazan los puntos en "trabajando..." en ms

/* fin constantes de configuraciones */

/* constantes funcionales */
enum states{
  STATE_PAUSA,
  STATE_TRABAJANDO,
  STATE_FINISH
};

/* fin constantes funcionales */


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////    INTERRUPCIONES    ////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* 
 *  Interrupcion externa, se utiliza para contar la cantidad de vueltas 
 */
void sensor_ISR(){

  if(trabajando == 1){ 
    vueltas++;
    if((cantVueltas-vueltas)< 5){                        //decremento la velocidad al minimo cuando me faltan 5 vueltas
      decVel();
    }
  }
}

/* 
 *  Interrupcion externa, se utiliza para leer los pulsos del encoder
 */

void rotary_ISR(){
  static int8_t lookup_table[] = {0,0,0,-1,0,0,1,0,0,1,0,0,-1,0,0,0};
  static uint8_t enc_val = 0;
  
  enc_val = enc_val << 2;
  enc_val = enc_val | ((PIND & 0b11000) >> 3);

  cont_rotary = cont_rotary + lookup_table[enc_val & 0b1111];

}
/*
 * interrupcion por desborde de timer 1, se utiliza para generar los pulsos del motor paso a paso
 */
void ISR_pulsos(){
  static volatile bool togglePulsos=0;

  if(togglePulsos){
    if(trabajando){
      digitalWrite(step1,HIGH);
    }
  }
  else{
      digitalWrite(step1,LOW);
  }
  togglePulsos=!togglePulsos;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////    FUNCIONES    ///////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* 
 *  Esta funcion se llama para incrementar velocidad, y convierte esa velocidad en cuentas del timer
 */
void incVel(void){
  int vel;
  if(velocidad< VEL_MAX){
     velocidad++;
  }
  vel = map(velocidad,1,10,VEL_MIN_CUENTAS,VEL_MAX_CUENTAS);
  Timer1.initialize(vel);
}

/* 
 *  Esta funcion se llama para decrementar velocidad, y convierte esa velocidad en cuentas del timer
 */
void decVel(void){
  int vel;
  if(velocidad > 1){
     velocidad--;
  }
  vel = map(velocidad,1,10,VEL_MIN_CUENTAS,VEL_MAX_CUENTAS);
  Timer1.initialize(vel);
}

/*
 * Esta funcion se llama para incrementar la cantidad de vueltas seteadas para el ovillo
 */
void incVueltas(int cant){
  
  if(cantVueltas > 99 && cantVueltas< VUELTAS_MAX){
    cantVueltas+=10;
  }
  else if(cantVueltas+cant < VUELTAS_MAX ){
    cantVueltas+=cant;
  }
  else if((cantVueltas+1) < VUELTAS_MAX){
    cantVueltas++;
  } 
  vueltas=0;                              //si se modifica la cantidad de vueltas se reinicia la cuentas, no queda otra
}
/*
 * Esta funcion se llama para decrementar la cantidad de vueltas seteadas para el ovillo
 */
void decVueltas(void){
  
  if(cantVueltas>100){
    cantVueltas-=10;
  }
  else if(cantVueltas>1){
    cantVueltas--;  
  }
  vueltas=0;                              //si se modifica la cantidad de vueltas se reinicia la cuentas, no queda otra
}

/*
 * muestro una animacion con la marca del producto
 */
void presentacion(){
int i,j,k,var7=0,temp=50;

  lcd.clear();
  for(j=0;j<2;j++){                                       
    for(i=0;i<16;i++){

      lcd.setCursor(i,j);
      lcd.print(">");
      var7= 15-i;
      lcd.setCursor(var7,j);
      lcd.print("<");
      delay(temp);

      if((j==1) & (i<5)){
        lcd.setCursor(4-i,0);
        lcd.print(" ");
        lcd.setCursor(11+i,0);
        lcd.print(" ");
      }
    }

    if(j == 0){
      for(k=0;k<3;k++){
      
        lcd.setCursor(8-k,0);
        lcd.print(" ");
        lcd.setCursor(8+k,0);
        lcd.print(" ");
        delay(temp);
      }
      lcd.setCursor(5,0);
      lcd.print(" OVIX");
    } 
  }
  for(i=7;i>=0;i--){
    lcd.setCursor(i,1);
    lcd.print(" "); 
    lcd.setCursor(8+(7-i),1);
    lcd.print(" "); 
    delay(temp);
  }
  delay(2000);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////     SETUP     ////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
int checksum=0;
  Timer1.attachInterrupt(ISR_pulsos);                                     //interrupcion de timer para controlar los pulsos del motor paso a paso;
  attachInterrupt(digitalPinToInterrupt(2), sensor_ISR, RISING);          //configuracion IRQ sensor de efecto hall
  attachInterrupt(digitalPinToInterrupt(3), rotary_ISR, CHANGE);          //configuracion IRQ para la lectura del encoder
  interrupts();                                                           //habilito interrupciones globales

  Serial.begin(115200);                                                   //solo para fines de debug

  pinMode(step1, OUTPUT);
  pinMode(dir1, OUTPUT);
  pinMode(enable_pin, OUTPUT);
  pinMode(backlight, OUTPUT);
  pinMode(vcc_sensor, OUTPUT);                                            //el sensor de efecto hall se alimenta desde es uC
  pinMode(rotary_A , INPUT_PULLUP);
  pinMode(rotary_B , INPUT_PULLUP);
  pinMode(sensor, INPUT_PULLUP);
  pinMode(enter , INPUT_PULLUP);

  digitalWrite(dir1,DIR_MOTOR);                                           //pongo el pin de direccion segun el define declarado
  digitalWrite(step1,LOW);                                            

  if(AUTO_DISABLE){                                                       //si esta el modo AUTO_DISABLE declarado como true inicio el programa con el driver DESHABILITADO
    digitalWrite(enable_pin,HIGH);
  }
  else{                                                                   //si esta el modo AUTO_DISABLE declarado como false inicio el programa con el driver HABILITADO
    digitalWrite(enable_pin,LOW);
  }
  
  digitalWrite(backlight, HIGH);                                          //alimento backlight
  digitalWrite(vcc_sensor,HIGH);                                          //alimento el sensor hall
  
  lcd.begin(16, 2);                                                       //inicializo display
  presentacion();

  Timer1.initialize(VEL_DEFAULT_CUENTAS);                                 //cargo en el timer 1 el periodo de vueltas default
    velocidad=VEL_DEFAULT;
    cantVueltas= VUELTAS_DEFAULT;
    cont_rotary=VUELTAS_DEFAULT;
    cont_ant=VUELTAS_DEFAULT;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////    MAIN    //////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void loop(void){
static int stateAct=STATE_PAUSA;

  if(!digitalRead(enter)){
    delay(LONGBUTTON);
    if(!digitalRead(enter)){
      if(stateAct!=STATE_TRABAJANDO){                       //no se pueden borrar las vueltas cuando uno esta trabajando
        vueltas=0;
        while(!digitalRead(enter));                           //espero a que se deje de presionar
      }
    }
    else{
      switch(stateAct){
        case STATE_PAUSA:
          stateAct=STATE_TRABAJANDO;
          velocidad=1;                                          //pongo en 0 la velocidad para el proximo inicio
          decVel();                                             //llamo a decVel para que me convierta la velocidad a cuentas del timer
          trabajando=1;
          if(AUTO_DISABLE){                                       //si esta definido como true habilita el motor al terminar
            digitalWrite(enable_pin,LOW);
          }
        break;
        case STATE_TRABAJANDO:
          stateAct=STATE_PAUSA;
          trabajando=0;
          cont_rotary=cantVueltas;
          cont_ant=cantVueltas;
          if(AUTO_DISABLE){                                       //si esta definido como true deshabilita el motor al terminar
            digitalWrite(enable_pin,HIGH);
          }
        break;
        case STATE_FINISH:
          stateAct=STATE_PAUSA;
        break;
      }
    }
  }

  switch(stateAct){
    case STATE_PAUSA:
      if((millis()-refresh1) >200){                        //tasa de refresco del display
        refresh1=millis(); 
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("     Pausa      ");
        if(cantVueltas>100){
          lcd.setCursor(0,1);
          lcd.print("vueltas:");
        }
        else{
          lcd.setCursor(1,1);
          lcd.print("vueltas: ");
        }
        lcd.print(vueltas);
        lcd.print("/");
        lcd.print(cantVueltas);
      }

      if(cont_rotary > cont_ant){
        incVueltas(abs(cont_rotary-cont_ant));
        cont_ant=cont_rotary;
      }
      else if((cont_rotary < cont_ant)){
        decVueltas();
        cont_ant=cont_rotary;
      }   
  
    break;
    case STATE_TRABAJANDO:
      
      if((cont_rotary > cont_ant) && abs(cont_rotary-cont_ant)>3){
        incVel();
        cont_ant=cont_rotary;
      }
      else if((cont_rotary < cont_ant) && abs(cont_rotary-cont_ant)>3){
        decVel();
        cont_ant=cont_rotary;
      }      

      if(vueltas >= cantVueltas){                           //si llegue a la cantidad de vueltas   
        stateAct=STATE_FINISH;                              
      }


      if((millis()-tPuntoAnt) > T_PUNTO){                   //cada T_PUNTO dubujo un punto mas en pantalla
          tPuntoAnt=millis();
          if(posPunto>2){
            posPunto=0;
            lcd.clear();                                    //cuando escribi los 3 '.' borro todo y vuelvo a empezar
          }
          posPunto++;     
      }
      
      lcd.setCursor(11+posPunto,0);
      lcd.print(".");
      lcd.setCursor(2,0);
      lcd.print("trabajando");
      if(cantVueltas>100){
        lcd.setCursor(0,1);
        lcd.print("vueltas:");
      }
      else{
        lcd.setCursor(1,1);
        lcd.print("vueltas: ");
      }
      
      lcd.print(vueltas);
      lcd.print("/");
      lcd.print(cantVueltas);   
    break;

    case STATE_FINISH:
      
      if(AUTO_DISABLE){                                       //si esta definido como true deshabilita el motor al terminar
        digitalWrite(enable_pin,HIGH);
      }
      vueltas=0;
      trabajando=0;
      if((millis()-refresh1) >200){                           //tasa de refresco del display
        refresh1=millis(); 
        lcd.clear();
        lcd.setCursor(5,0);
        lcd.print("OVILLO");
        lcd.setCursor(3,1);
        lcd.print("TERMINADO");
      }
    break;
  }
}
       
